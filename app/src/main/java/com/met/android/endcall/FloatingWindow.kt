package com.met.android.endcall

import android.content.Context
import android.graphics.PixelFormat
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import kotlinx.android.synthetic.main.activity_floating_window.*
import kotlinx.android.synthetic.main.content_floating_window.*

class FloatingWindow : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_floating_window)
        setSupportActionBar(toolbar)

        button.setOnClickListener {
            showFloatingWindow()
        }
    }

    private lateinit var floatingView: View
    private lateinit var collapsedView: View
    private lateinit var expandedView: View
    private lateinit var params: WindowManager.LayoutParams

    private fun showFloatingWindow() {
        floatingView = LayoutInflater.from(this).inflate(R.layout.content_caller_id, null)

        params = WindowManager.LayoutParams(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY,
            WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
            PixelFormat.TRANSLUCENT
        )

        val windowManager = getSystemService(Context.WINDOW_SERVICE) as WindowManager

        windowManager.addView(floatingView, params)

        floatingView.findViewById<ConstraintLayout>(R.id.container)
            .setOnTouchListener(
                object : View.OnTouchListener {

                    var X_Axis: Int = 0
                    var Y_Axis: Int = 0
                    var TouchX: Float = 0.toFloat()
                    var TouchY: Float = 0.toFloat()

                    override fun onTouch(v: View?, event: MotionEvent?): Boolean {
                        event?.let {
                            when (event.getAction()) {

                                MotionEvent.ACTION_DOWN -> {
                                    X_Axis = params.x
                                    Y_Axis = params.y
                                    TouchX = event.getRawX()
                                    TouchY = event.getRawY()
                                    return true
                                }

                                MotionEvent.ACTION_UP -> {
                                    return true
                                }

                                MotionEvent.ACTION_MOVE -> {
                                    params.x = X_Axis + (event.getRawX() - TouchX).toInt()
                                    params.y = Y_Axis + (event.getRawY() - TouchY).toInt()
                                    windowManager.updateViewLayout(floatingView, params)
                                    return true
                                }

                                else -> {
                                    return false
                                }
                            }
                        }
                        return false
                    }

                }
            )
    }
}
