package com.met.android.endcall

import android.app.job.JobParameters
import android.app.job.JobService

class JobServiceForPhone : JobService() {
    override fun onStopJob(params: JobParameters?): Boolean {
        return true
    }

    override fun onStartJob(params: JobParameters?): Boolean {
        return false
    }
}