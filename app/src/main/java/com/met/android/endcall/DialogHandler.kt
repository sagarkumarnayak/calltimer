package com.met.android.endcall

import android.app.Service
import android.content.Context
import android.content.Intent
import android.graphics.PixelFormat
import android.os.IBinder
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.WindowManager
import android.widget.Button

class DialogHandler : Service() {

    private lateinit var mDialogView: View
    private lateinit var mWindowManager: WindowManager

    override fun onBind(intent: Intent): IBinder {
        TODO("Return the communication channel to the service.")
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        //Create Class Level Variable  or as per requirement
        if (this::mDialogView.isInitialized) {
            removeDialog()
        } else {
            showDialog()
        }
        return super.onStartCommand(intent, flags, startId)
    }

    private fun showDialog() {
        Log.i(MainActivity.TAG, "show dialog called..")

        val mWindowsParams: WindowManager.LayoutParams = WindowManager.LayoutParams(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY,
            WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
            PixelFormat.TRANSLUCENT
        )
        mWindowManager =
            getSystemService(Context.WINDOW_SERVICE) as WindowManager
        mDialogView = LayoutInflater.from(this).inflate(
            R.layout.activity_caller_id,
            null,
            false
        )

        //initialize variable

        //Add Window on your event
        mWindowManager.addView(mDialogView, mWindowsParams)

        val button = mDialogView.findViewById<Button>(R.id.button)

        button.setOnClickListener {
            removeDialog()
        }
    }

    private fun removeDialog() {
        Log.i(MainActivity.TAG, "remove dialog called..")
        //For Remove window
        try {
            if (mDialogView.parent != null) {
                mWindowManager.removeViewImmediate(mDialogView)
            }
        } catch (e: Exception) {
            Log.i(MainActivity.TAG, "remove dialog error.. $e")
        }

        stopSelf()
    }
}
