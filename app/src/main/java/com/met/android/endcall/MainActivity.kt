package com.met.android.endcall

import android.Manifest
import android.annotation.SuppressLint
import android.app.Service
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.provider.Settings
import android.telecom.TelecomManager
import android.telephony.TelephonyManager
import android.util.Log
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.judemanutd.autostarter.AutoStartPermissionHelper
import com.met.android.endcall.databinding.ActivityMainBinding
import permissions.dispatcher.*


@RuntimePermissions
class MainActivity : AppCompatActivity() {

    companion object {
        const val TAG = "sfbdbndsgndsgnd"
    }

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        binding = DataBindingUtil.setContentView(
            this,
            R.layout.activity_main
        )
        binding.context = this
        setSupportActionBar(binding.toolbar)

        getPermissionsWithPermissionCheck()

        askForOverLayPermission()

        if (
            AutoStartPermissionHelper.getInstance().isAutoStartPermissionAvailable(this)
        ) {
            Toast.makeText(this, "available", Toast.LENGTH_SHORT).show()
            AutoStartPermissionHelper.getInstance().getAutoStartPermission(this)
        } else {
            Toast.makeText(this, "not available", Toast.LENGTH_SHORT).show()
            AutoStartHelper().getAutoStartPermission(this)
        }
    }

    @SuppressLint("DiscouragedPrivateApi")
    fun closeCall() {
        Log.i(TAG, "going to close call now")

        val service = getSystemService(Service.TELECOM_SERVICE) as TelecomManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            @Suppress("DEPRECATION") val result = service.endCall()
            Log.i(TAG, "the result is $result using end call")
        } else {
            try {
                val tm = getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager

                val m1 = tm.javaClass.getDeclaredMethod("getITelephony")
                m1.isAccessible = true
                val iTelephony = m1.invoke(tm)

                @Suppress("UNUSED_VARIABLE") val m2 =
                    iTelephony.javaClass.getDeclaredMethod("silenceRinger")
                val m3 = iTelephony.javaClass.getDeclaredMethod("endCall")

//                m2.invoke(iTelephony)
                m3.invoke(iTelephony)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            Log.i(TAG, "used the iTelephony package")
        }
    }

    @NeedsPermission(
        Manifest.permission.CALL_PHONE,
        Manifest.permission.READ_PHONE_STATE
    )
    fun getPermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            getPhoneCallPermissionsWithPermissionCheck()
    }

    @RequiresApi(Build.VERSION_CODES.O)
    @NeedsPermission(
        Manifest.permission.ANSWER_PHONE_CALLS
    )
    fun getPhoneCallPermissions() {
    }

    @OnShowRationale(
        Manifest.permission.CALL_PHONE,
        Manifest.permission.READ_PHONE_STATE
    )
    fun showRationale(@Suppress("UNUSED_PARAMETER") request: PermissionRequest) {
        Toast.makeText(
            this,
            "We need few permission.",
            Toast.LENGTH_LONG
        ).show()
    }

    @RequiresApi(Build.VERSION_CODES.O)
    @OnShowRationale(
        Manifest.permission.ANSWER_PHONE_CALLS
    )
    fun showRationalePhoneCall(@Suppress("UNUSED_PARAMETER") request: PermissionRequest) {
        Toast.makeText(
            this,
            "We need few permission.",
            Toast.LENGTH_LONG
        ).show()
    }

    @OnPermissionDenied(
        Manifest.permission.CALL_PHONE,
        Manifest.permission.READ_PHONE_STATE
    )
    fun permissionDenied() {
        Toast.makeText(
            this,
            "We need few permission.",
            Toast.LENGTH_LONG
        ).show()
    }

    @RequiresApi(Build.VERSION_CODES.O)
    @OnPermissionDenied(
        Manifest.permission.ANSWER_PHONE_CALLS
    )
    fun permissionDeniedPhoneCall() {
        Toast.makeText(
            this,
            "We need few permission.",
            Toast.LENGTH_LONG
        ).show()
    }

    @OnNeverAskAgain(
        Manifest.permission.CALL_PHONE,
        Manifest.permission.READ_PHONE_STATE
    )
    fun neverAskAgain() {
        Toast.makeText(
            this,
            "We need few permission.",
            Toast.LENGTH_LONG
        ).show()
    }

    @RequiresApi(Build.VERSION_CODES.O)
    @OnNeverAskAgain(
        Manifest.permission.ANSWER_PHONE_CALLS
    )
    fun neverAskAgainPhoneCall() {
        Toast.makeText(
            this,
            "We need few permission.",
            Toast.LENGTH_LONG
        ).show()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        onRequestPermissionsResult(requestCode, grantResults)
    }

    private fun askForOverLayPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!Settings.canDrawOverlays(this)) {
                val intent = Intent(
                    Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                    Uri.parse("package:$packageName")
                )
                startActivityForResult(intent, 1234)
                waitForUserToAcceptPermission()
            }
        }
    }

    private fun waitForUserToAcceptPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (Settings.canDrawOverlays(this)) {
                startActivity(
                    Intent(
                        this,
                        MainActivity::class.java
                    )
                )
                finish()
            } else {
                Handler().postDelayed(
                    {
                        waitForUserToAcceptPermission()
                    },
                    1000
                )
            }
        }
    }

    fun startFloatingWindow() {
        startActivity(
            Intent(
                this,
                FloatingWindow::class.java
            )
        )
    }
}
