package com.met.android.endcall

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.view.Window
import androidx.appcompat.app.AppCompatActivity

class CallerId : AppCompatActivity() {

    private val broadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            finish()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        try {
            requestWindowFeature(Window.FEATURE_NO_TITLE)
            this.setFinishOnTouchOutside(false)
            super.onCreate(savedInstanceState)
            setContentView(R.layout.content_caller_id)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        registerReceiver(
            broadcastReceiver,
            IntentFilter(
                "close"
            )
        )
    }
}
