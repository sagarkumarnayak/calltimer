package com.met.android.endcall

import android.annotation.SuppressLint
import android.app.Application
import android.content.Context
import android.graphics.PixelFormat
import android.os.Build
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.WindowManager
import android.widget.Button
import android.widget.Toast
import androidx.coordinatorlayout.widget.CoordinatorLayout

class Application : Application() {

    private lateinit var mDialogView: View
    private lateinit var mWindowManager: WindowManager
    private lateinit var mWindowsParams: WindowManager.LayoutParams
    private var isShowing = false
    private var xAxis = 0
    private var yAxis = 0
    private var touchX = 0f
    private var touchY = 0f

    override fun onCreate() {
        super.onCreate()

        prepareDialog()
    }

    fun perform() {
        Toast.makeText(
            this,
            "Pefrorm",
            Toast.LENGTH_SHORT
        ).show()
        if (isShowing) {
            removeDialog()
        } else {
            showDialog()
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun prepareDialog() {
        Toast.makeText(
            this,
            "prepare",
            Toast.LENGTH_SHORT
        ).show()
        Log.i(MainActivity.TAG, "prepare dialog called..")

        mWindowsParams = WindowManager.LayoutParams(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.WRAP_CONTENT,
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY
            else
                WindowManager.LayoutParams.TYPE_PHONE,
            WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE or
                    WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED or
                    WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD or
                    WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON,
            PixelFormat.TRANSLUCENT
        )
        mWindowManager =
            getSystemService(Context.WINDOW_SERVICE) as WindowManager
        mDialogView = LayoutInflater.from(this).inflate(
            R.layout.activity_caller_id,
            null,
            false
        )

        //initialize variable

        val button = mDialogView.findViewById<Button>(R.id.button)

        button.setOnClickListener {
            removeDialog()
        }

        mDialogView.findViewById<CoordinatorLayout>(R.id.activityContainer)
            .setOnTouchListener(
                object : View.OnTouchListener {
                    override fun onTouch(v: View?, event: MotionEvent?): Boolean {
                        event?.let {
                            return when (event.action) {
                                MotionEvent.ACTION_DOWN -> {
                                    xAxis = mWindowsParams.x
                                    yAxis = mWindowsParams.y
                                    touchX = it.rawX
                                    touchY = it.rawY
                                    true
                                }
                                MotionEvent.ACTION_UP -> {
                                    true
                                }
                                MotionEvent.ACTION_MOVE -> {
                                    mWindowsParams.x = xAxis + (it.rawX - touchX).toInt()
                                    mWindowsParams.y = yAxis + (it.rawY - touchY).toInt()
                                    mWindowManager.updateViewLayout(
                                        mDialogView, mWindowsParams
                                    )
                                    true
                                }
                                else -> {
                                    false
                                }
                            }
                        }
                        return false
                    }
                }
            )
    }

    private fun showDialog() {
        Toast.makeText(
            this,
            "show",
            Toast.LENGTH_SHORT
        ).show()
        Log.i(MainActivity.TAG, "add dialog called..")
        try {
            Handler().postDelayed(
                {
                    mWindowManager.addView(mDialogView, mWindowsParams)
                },
                500
            )
        } catch (e: Exception) {
            Log.i(MainActivity.TAG, "add dialog error.. $e")
        }

        isShowing = true
    }

    private fun removeDialog() {
        Toast.makeText(
            this,
            "hide",
            Toast.LENGTH_SHORT
        ).show()
        Log.i(MainActivity.TAG, "remove dialog called..")
        //For Remove window
        try {
            mWindowManager.removeViewImmediate(mDialogView)
        } catch (e: Exception) {
            Log.i(MainActivity.TAG, "remove dialog error.. $e")
        }

        isShowing = false
    }

}