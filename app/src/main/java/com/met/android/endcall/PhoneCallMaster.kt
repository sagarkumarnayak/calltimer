package com.met.android.endcall

import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.telephony.TelephonyManager
import android.util.Log
import android.widget.Toast

class PhoneCallMaster : BroadcastReceiver() {
    @SuppressLint("UnsafeProtectedBroadcastReceiver")
    override fun onReceive(context: Context?, intent: Intent?) {
        Toast.makeText(
            context,
            "received",
            Toast.LENGTH_SHORT
        ).show()
        Log.i(MainActivity.TAG, "started to listen for phone state")

        intent?.let {
            when (
                it.getStringExtra(
                    TelephonyManager.EXTRA_STATE
                )
                ) {
                TelephonyManager.EXTRA_STATE_RINGING -> {
                    Log.i(MainActivity.TAG, "phone is ringing...")
                    context?.let { contextForDialog ->
                        showDialog(contextForDialog)
                    }
                }
                TelephonyManager.EXTRA_STATE_OFFHOOK -> {
                    Log.i(MainActivity.TAG, "off hook ...")
                    context?.let { contextForDialog ->
                        removeDialog(contextForDialog)
                    }
                }
                TelephonyManager.EXTRA_STATE_IDLE -> {
                    Log.i(MainActivity.TAG, "phone is idle ...")
                    context?.let { contextForDialog ->
                        removeDialog(contextForDialog)
                    }
                }
                else -> {
                    Log.i(MainActivity.TAG, "don't know what state is this !!!!!")
                }
            }
        }
    }

    @SuppressLint("InflateParams")
    private fun showDialog(context: Context) {
        Toast.makeText(
            context,
            "sending show",
            Toast.LENGTH_SHORT
        ).show()
        //For Remove window
        /* Handler().postDelayed(
             {
                 context.startActivity(
                     Intent(
                         context,
                         CallerId::class.java
                     )
                         .addFlags(
                             Intent.FLAG_ACTIVITY_NEW_TASK or
                                     Intent.FLAG_ACTIVITY_SINGLE_TOP
                         )
                         .putExtra(
                             "action",
                             "open"
                         )
                 )
             },
             100
         )*/

        /*Handler().postDelayed(
            {
                context.startService(
                    Intent(
                        context,
                        DialogHandler::class.java
                    )
                )
            },
            100
        )*/

        (context.applicationContext as Application).perform()


    }

    private fun removeDialog(context: Context) {
        Toast.makeText(
            context,
            "sending remove",
            Toast.LENGTH_SHORT
        ).show()
        //For Remove window
        /*Handler().postDelayed(
            {
                context.startActivity(
                    Intent(
                        context,
                        CallerId::class.java
                    )
                        .addFlags(
                            Intent.FLAG_ACTIVITY_NEW_TASK or
                                    Intent.FLAG_ACTIVITY_SINGLE_TOP
                        )
                        .putExtra(
                            "action",
                            "close"
                        )
                )
            },
            100
        )*/

        /*Handler().postDelayed(
            {
                context.startService(
                    Intent(
                        context,
                        DialogHandler::class.java
                    )
                )
            },
            100
        )*/

        /*context.sendBroadcast(
            Intent("close")
        )*/

        (context.applicationContext as Application).perform()

    }
}